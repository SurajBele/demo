#SIMPLE APACHE2 WEB SERVER ON UBUNTU
------------------------------------------------
# FROM ubuntu:20.04
# RUN apt-get update
# ENV DEBIAN_FRONTEND=noninteractive
# RUN apt-get install apache2 -y
# RUN echo "<h1> Hello Docker</h1>" > /var/www/html/index.html
# RUN apt-get clean
# EXPOSE 80
# CMD ["apache2ctl", "-D", "FOREGROUND"]
=========================================================================

TOMCAT SERVER ON UBUNTU
----------------------------------------------------
# FROM ubuntu:20.04
# ENV CATALINA_HOME=/opt/apache-tomcat-9.0.97
# ENV PATH=$CATALINA_HOME/bin:$PATH
# RUN apt-get update
# RUN apt-get install default-jdk -y 
# ADD https://dlcdn.apache.org/tomcat/tomcat-9/v9.0.97/bin/apache-tomcat-9.0.97.tar.gz /opt/
# WORKDIR /opt/
# RUN tar -xzf apache-tomcat-9.0.97.tar.gz
# #WORKDIR /opt/apache-tomcat-9.0.97/bin/
# EXPOSE 8080
# CMD ["catalina.sh", "run"]
===============================================================================

TOMCAT WITH JAVA BASE IMAGES
-------------------------------------------
# Use a slim base image with Java pre-installed
FROM openjdk:11-jdk-slim

# Set environment variables for Tomcat
ENV CATALINA_HOME=/opt/apache-tomcat-9.0.97
ENV PATH=$CATALINA_HOME/bin:$PATH

# Install required dependencies and clean up
RUN apt-get update && apt-get install -y curl && rm -rf /var/lib/apt/lists/*

# Download and extract Tomcat
WORKDIR /opt
ADD -fSL https://dlcdn.apache.org/tomcat/tomcat-9/v9.0.97/bin/apache-tomcat-9.0.97.tar.gz -o apache-tomcat-9.0.97.tar.gz && \
    tar -xzf apache-tomcat-9.0.97.tar.gz && \
    rm apache-tomcat-9.0.97.tar.gz

# Remove unnecessary directories to reduce image size
RUN rm -rf $CATALINA_HOME/webapps/examples $CATALINA_HOME/webapps/docs

# Expose Tomcat's HTTP port
EXPOSE 8080

# Start Tomcat in the foreground
CMD ["catalina.sh", "run"]
================================================================